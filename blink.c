#include <msp430.h>
#include <stdint.h>

static void __attribute__((naked, section(".crt_0042"), used))
disable_watchdog (void)
{
  WDTCTL = WDTPW | WDTHOLD;
}


int main(void) {
	// WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer // See function above
	P1DIR |= BIT5;					// Set P1.0 to output direction

	for(;;) {
		volatile uint32_t i;	// volatile to prevent optimization

		P1OUT ^= BIT5;				// Toggle P1.0 using exclusive-OR

		i = 100000;					// SW Delay
		do i--;
		while(i != 0);
	}
	
	return 0;
}
