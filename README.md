# Test MSP 430 Project
## Requirements
* The [TI MSP430 GCC toolchain](http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/index_FDS.html)
  * Visit link
  * Make TI account
  * Download the Full Installer
  * Note the installed directory
* `mspdebug`
* `build-essential` (`make`, etc...)

## Building
`make MSP430_TOOLCHAIN_PATH=/path/to/msp430-gcc`

## Flashing
`make flash`
