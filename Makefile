OBJECTS=blink.o

MSP430_TOOLCHAIN_PATH ?= ./msp430-gcc
GCC_DIR =  $(MSP430_TOOLCHAIN_PATH)/bin
SUPPORT_FILE_DIRECTORY = $(MSP430_TOOLCHAIN_PATH)/include

# Please set your device here
DEVICE         = msp430g2230
CC             = $(GCC_DIR)/msp430-elf-gcc
GDB            = $(GCC_DIR)/msp430-elf-gdb
MSPDEBUG       = mspdebug
MSPDEBUGDRIVER = rf2500

CFLAGS = -I $(SUPPORT_FILE_DIRECTORY) -mmcu=$(DEVICE) -O2 -g
LFLAGS = -L $(SUPPORT_FILE_DIRECTORY) -T $(DEVICE).ld

.PHONY: clean debug flash build_and_run

all: ${OBJECTS}
	$(CC) $(CFLAGS) $(LFLAGS) $? -o $(DEVICE).out

clean:
	rm -f *.o *.out

debug: all
	$(GDB) $(DEVICE).out

flash: all
	$(MSPDEBUG) $(MSPDEBUGDRIVER) "prog $(DEVICE).out" exit

build_and_run: all
	$(MAKE) flash
